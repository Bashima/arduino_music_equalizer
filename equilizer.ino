/*
 Mic is connected to arduino analog pin 2. red green blue leds are connected to digital pins of arduino.
 */

#define microphonePin A2
#define Red 3      
#define Green 5
#define Blue 6
int sound;

void setup() {
  // put your setup code here, to run once:
  Serial.begin(9600);
  pinMode(microphonePin, INPUT);
  pinMode(Red, OUTPUT);
  pinMode(Green, OUTPUT);
  pinMode(Blue, OUTPUT);
}

void loop() {
   sound1= analogRead(microphonePin);  // getting input audio signal readings
  
  Serial.print(sound1);    // input audio signal readings to Serial display
  Serial.print(" ");
     
   //calibrate the values in this these conditions by looking at the values in the serial display  
   if(sound1>240)
        {
             digitalWrite(Red,HIGH);   // set the LEDs on
             digitalWrite(Green,HIGH);
             digitalWrite(Blue,HIGH);
        }  
        else if((sound1)>200)
        {
             digitalWrite(Red,LOW);   // set the LED off
             digitalWrite(Green,HIGH);
             digitalWrite(Blue,HIGH);
            
        }
        else if(sound1>160)
        {
             digitalWrite(Red,HIGH);   // set the LED on
             digitalWrite(Green,LOW);
             digitalWrite(Blue,HIGH);   // set the LED on
             
        }
         else if(sound1>120)
        {
             digitalWrite(Red,LOW);   // set the LED off
             digitalWrite(Green,LOW);   // set the LED off
             digitalWrite(Blue,HIGH);
             
        }
        
         else if(sound1>80)
        {
             digitalWrite(Red,HIGH);   // set the LED on
             digitalWrite(Green,HIGH);   // set the LED on
             digitalWrite(Blue,LOW);
             
        }
          else if(sound1>40)
        {
             digitalWrite(Red,LOW);   // set the LED on
             digitalWrite(Green,HIGH);   // set the LED off
             digitalWrite(Blue,LOW);
             
        }
          else if(sound1>10)
        {
             digitalWrite(Red,HIGH);   // set the LED on
             digitalWrite(Green,LOW);   // set the LED off
             digitalWrite(Blue,LOW);
             
        }
         else
        {
             digitalWrite(Red,LOW);   // set the LEDs off
             digitalWrite(Green,LOW);   // set the LEDs off
             digitalWrite(Blue,LOW);   // set the LEDs off
            
        }

  Serial.println(sound1);
 
  delay(10);
}